# BYU Automated RF Characterizer

Automated RF characterizer for use with HP 8592A Spectrum Analyzers.
Sets up the frequency and power ranges and trace configuration.
Executes various GPIB command sets to simplify data gathering.
Copies data to clipboard for quick pasting into Microsoft Excel.

Hotkeys:
 * a - download Trace A and copy into clipboard
 * b - download Trace B and copy into clipboard
 * c - download both Trace A and Trace B and copy into clipboard (tab-separated columns)
 * 1 - Trace A CLEAR / WRITE
 * 2 - Trace A MAX HOLD
 * 3 - TRACE A VIEW
 * 4 - Trace A STORE / BLANK
 * 5 - Trace B CLEAR / WRITE
 * 6 - Trace B MAX HOLD
 * 7 - TRACE B VIEW
 * 8 - Trace B STORE / BLANK
 * 0 - Transmit "stb?" to clear error messages
 * l - Enable local control ("++loc")
 * i - Request instrument ID ("id?")
 * q - Toggle amplitude scale between 0 / +30 dBm (for use without / with 30 dB attenuator, respectively)
 
 

Implemented with/for:
 * OpenFrameworks v0.9.3
 * Microsoft Visual Studio Professional 2015
 * Microsoft Windows 7 / 10
 * NVIDIA (R) Quadro (R) FX 5800 graphics card, for its dual DVI-__**I**__ outputs
 * Prologix USB-GPIB Adapter
 * Hewlett Packard 8592A Spectrum Analyzer
 
# License

All files in this repository, including source code and auxiliary references are Copyright 2018 BYU ElectroHolography Research Group and are licensed under GNU General Public License v3 ([https://www.gnu.org/licenses/gpl-3.0.en.html](https://www.gnu.org/licenses/gpl-3.0.en.html)).

![GPL v3 Logo](https://www.gnu.org/graphics/gplv3-127x51.png "GPL v3")
