#pragma once

#include "ofMain.h"

// HP8592A has 401 points
#define N_SWEEP_DATA 401

using namespace std;

class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);
		
    void ofApp::open_GPIB_COM_port();

    ofSerial GPIB_COM;
    int GPIB_COM_port_ID;

    //bool append_carriage_return;
    //bool append_line_feed;

    typedef enum Command
    {
      COMMAND_NONE,
      COMMAND_START_SWEEP_A,
      COMMAND_START_SWEEP_B,
      COMMAND_SWEEP_GATHER_A,
      COMMAND_SWEEP_GATHER_B,
      COMMAND_SWEEP_REPORT_A,
      COMMAND_SWEEP_REPORT_B,
      COMMAND_CLIPBOARD_A,
      COMMAND_CLIPBOARD_B,
      COMMAND_CLIPBOARD_AB
    } Command;

    Command executing_command;
    uint64_t  command_last_active_time;
    uint64_t  command_timeout_ms = 500;
    bool command_timeout;
    Command last_command;

    list<Command> command_list;

    string received_data;
    int comma_counter;

    float sweep_data[2][ N_SWEEP_DATA ]; // trace A & B
    bool attenuate_30dB;
};
